# Lancer un scénario

Après avoir pu [télécharger les fichiers](./telecharger.md) il suffit d'ouvrir le fichier 

   src/index.html

* Les identifiants des scénarios correspondent au nom de leur répertoire dans __src/scenarios__
* Les scénarios sont référencés dans le fichier __src/scenarios/playlist.json__

On peut automatiquement charger le premier scénario de la playlits avec l'option __load=true__

    src/index.html?load=true

On peut automatiquement charger un scénario en donnant le nom de son fichier descriptif comme ici :

    src/index.html?scenario=scenarios/exploration-visuelle/scenario.wjson
