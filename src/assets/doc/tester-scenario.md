# Tester son scénario

Après avoir pu [télécharger les fichiers](./telecharger.md), [installer un nouveau scénario](./creer-scenario.md), et [programmer son scénario](./faire-scenario.md) voici comment _tester son scénario_.

## Mise en place du test

Le test se fait en lançant le scénario en mode test par le menu de _neurosmart/src/index.html_ ou en utilisant le lien

    neurosmart/src/index.html?scenario=scenarios/$identifant-du-scenario/scenario.wjson&__debug__=true

où _$identifant-du-scenario_ est par exemple _exploration-visuelle_ et en ouvrant la console javascript du navigateur qui va produire des messages au fut et à mesure du déroulement du scénario.

## Déroulement du test

L'interpréteur de scénario va :

1. Lire le fichier en weak-JSON et le convertir en structure de donnée
2. Véifier que le contenu de ce fichier correspond au [schéma attendu](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/src/assets/etc/schema.json)
3. Exécuter le scénario au fur et à mesure des clics de l'utilisateur et signalr toute anomalie (par exemple, l'absence d'u nfichier)

## Comment mener le test ?

1. Ouvrir son fichier _scenario.wjson_ dans son éditeur de texte usuel
2. Afficher scénario en mode test et le faire fonctionner
3. Quand quelque chose est à changer (corriger une erreur, revoir un élément) :
  * Faire la corrections dans le fichier _scenario.wjson_ et sauvegarder
  * Recharcher la page qui lance le scénario en mode test et le refaire fonctionner

## Quelques conseils supplémentaires

* On peut lancer un scénario à partir d'une séquence donnée, sans revenir au debut, par exemple dans le [scénario d'exploration visuelle](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/src/scenarios/exploration-visuelle/scenario.wjson) on peut démarrer à partir de la séquence _le-thalamus_ en utilisant le lien 

        src/index.html?scenario=scenarios/exploration-visuelle/scenario.wjson&__where__=le-thalamus
