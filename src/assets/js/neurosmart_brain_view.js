"use strict";

var NS = NS || {};

/**
 * Main view
 *
 * Javascript object controlling the 3D View
 *
 */

NS.BrainView = {

	/**
	 * Initialization
	 * - adds the ns-view CSS class to be retrievable by the main controler NS.Controler
	 * - establishes a relation between the DOM element of the HTML content view and the Javascript class NS.ContentView
	 */
	init: function () {
		$(this.domElement).addClass("ns-view");
		$(this.domElement).data("controler", this);
	},

	/**
	 * Lists all the available commands of the Brain view
	 * @return [{array}] - An array of the supported commands
	 */
	availableCommands: function () {
		return [
			"setOpacity",
			"setColor",
			"setCamera",
			"pointAt",
			"pointAtArea",
			"restoreColor",
			"hilite",
			"hiliteArea",
			"hiliteAndPointAt",
			"hiliteAndPointAtArea",
			"restoreAllColors",
			"restoreAllAreas",
			"restoreBrainView"
		];
	},

	/**
	 * Loads a JSON file describing the model and its properties :
	 * - generic ID of submodels
	 * - nale of submodels in the .obj file
	 * - rendering order of submodels
	 * - position of the camera when running the command "pointAt" with a submodel ID reference
	 * - ID of areas,
	 * - colors of each area in the colored-texture ( used by the filter by color through shader)
	 *
	 * Ex : models3D/default/brain.json
	 *
	 * @param {string} playlistUrl - URL of the JSON file describing the model
	 */

	loadModelDescription: function (brainJsonURL) {

		this._brainUrl = brainJsonURL ? brainJsonURL : "models3d/default/brain.json";

		var pathArray = this._brainUrl.split('/');
		pathArray.pop();

		this._brainFolder = pathArray.join('/') + '/';
		this._isLoaded = false;

		// Début du chargement du fichier JSON
		NS.Utilities.asyncLoadTextFile(this._brainUrl, this, this.loadModelDescriptionComplete, this.loadModelDescriptionError);
	},

	/**
	 * Callback error called when JSON model description file could not be loaded
	 * @param {string} notFoundSchemaUrl - URL of the file describing the JSON schema
	 */

	loadModelDescriptionError: function (notFoundUrl) {
		console.log("JSON for model description not found", notFoundUrl);
	},

	/**
	 * Callback event called when JSON model description is fully loaded
	 * @param {string} modelDescriptionJson - JSON file content
	 */

	loadModelDescriptionComplete: function (modelDescriptionJson) {

		var brainJson = JSON.parse(modelDescriptionJson);

		// Json config file defines an associative array :
		// . brain zone "id" ( for scenarii ) -> group name in 3D Model .obj file
		// . optional parameters : color, colorPercent, opacity
		this._objectsState = brainJson.objects;
		var i, n = this._objectsState.length;
		var objectIdToObjectIn3DModel = [], stateObj;
		for (i = 0; i < n; i++) {
			stateObj = this._objectsState[i];
			objectIdToObjectIn3DModel[stateObj.id] = stateObj;
		}
		this._sceneIdToObject = objectIdToObjectIn3DModel;

		var areasInfos = brainJson.areas;
		var areaIdToAreaInfos = [], areaObj;
		for (i = 0; i < areasInfos.length; i++) {
			areaObj = areasInfos[i];
			areaIdToAreaInfos[areaObj.id] = areaObj;
		}
		this._areaIdToAreaInfos = areaIdToAreaInfos;

		this._sceneObjects = { };

		this.create3DScene();
		this.load3DModel(brainJson.model, brainJson.texture, brainJson.textureColor);
	},

	/* Private functions */

	get3DObjectParametersFromId: function ( submodelID ) {
		return this._sceneIdToObject[submodelID];
	},

	getAreaInfosFromId: function (areaId) {
		var areaParameters = this._areaIdToAreaInfos[areaId];
		if (!areaParameters) {
			this.logError("La zone " + areaId + " n'est pas reconnue !");
		} else {
			return areaParameters;
		}
	},

	/**
	 * Returns a object with the parameter from the JSON model description file
	 * Logs an error if the subModelID doesn't exist
	 *
	 * @param {string} submodelID - generic ID of submodel of the brain model ( the "real" name int the obj file could be different )
	 * @return [{object}] - parameter from the JSON model description file
	 */
	get3DObjectFromId: function (submodelID) {

		// Parameters from brainJson :
		var objectParameters = this.get3DObjectParametersFromId(submodelID);

		if (!objectParameters) {
			this.logError("L'élément 3D " + submodelID + " n'est pas reconnu !");
		} else {
			// Real name in 3D Model
			var objectName = objectParameters.name;
			if (objectName) {
				// 3D Object in 3D Model
				return this._sceneObjects[objectName];
			}
		}
	},

	/**
	 * Dispatch an event that, if captured by the controller, could be displayed in the log view
	 */
	logError: function (info, obj) {
		this.domElement.dispatchEvent(new CustomEvent("logError", {
			bubbles: true,
			cancelable: true,
			detail: {
				info: info,
				obj: obj
			}
		}));
	},

	/**
	 * Create ThreeJS entities, initializes the trackball control, and starts the rendering loop
	 */

	create3DScene: function () {

		this._scene = new THREE.Scene();
		this._renderer = new THREE.WebGLRenderer({alpha: true});

		this._camera = new THREE.PerspectiveCamera(30, 1, 0.1, 100);

		var ambientLight = new THREE.AmbientLight( 0xcccccc, 0.5 );
		this._scene.add( ambientLight );

		var pointLight = new THREE.PointLight( 0xffffff, 0.8 );
		this._camera.add( pointLight );

		this._scene.add(this._camera);

		this.origin = new THREE.Vector3(0, 0, 0);

		// Controls setup
		this._controls = new THREE.TrackballControls(this._camera, this.domElement);
		this._controls.rotateSpeed = 3.0;
		this._controls.zoomSpeed = 2;
		this._controls.panSpeed = 2;
		this._controls.noZoom = false;
		this._controls.noPan = true;
		this._controls.staticMoving = true;
		this._controls.maxDistance = 20;
		this._controls.minDistance = 2;

		/*
		this._controls = new THREE.OrbitControls(this._camera, this.domElement, 1, 10);
		this._controls.minDistance = 2;
		this._controls.maxDistance = 5;
		this._controls.enableZoom = true;
		*/

		var t = this;

		this._controls.addEventListener('change', function () {
			t.onChangeControls();
		}, false);

		this._camera.position.x = 0;
		this._camera.position.y = 0;
		this._camera.position.z = 5;
		this._camera.lookAt(this.origin);

		// On mémorise la position initiale
		this.cameraPos0 = this._camera.position.clone();
		this.cameraUp0 = this._camera.up.clone();
		this.cameraQ = new THREE.Quaternion();

		this._renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);
		this._renderer.setClearColor(0x000000, 0);

		this.domElement.appendChild(this._renderer.domElement);

		// Event Handlers
		window.onresize = function () {
			t._renderer.setSize(t.domElement.clientWidth, t.domElement.clientHeight);
		};



	},

	startRendering:function() {

		var t = this;
		t._firstRender = false;

		// Animation loop
		function animate(time) {
			requestAnimationFrame(animate);
			TWEEN.update(time);
			t._controls.update();
			t._renderer.render(t._scene, t._camera);

			if ( ! t._firstRender ) {
				t._firstRender = true;
				t.domElement.dispatchEvent(new CustomEvent("brainRenderComplete", {
					bubbles: true,
					cancelable: true
				}));
			}
		}

		requestAnimationFrame(animate);
	},

	/**
	 * Callback event called when the 3D model is manipulated on screen
	 * @param {object} e - event
	 */

	onChangeControls: function (e) {

		var camera = this._camera;
		if (camera) {
			var cameraQ = camera.quaternion;
			var cameraEuler = new THREE.Euler().setFromQuaternion(cameraQ, 'XYZ');
			var cameraDistance = camera.position.length();

			this.domElement.dispatchEvent(new CustomEvent("cameraChange", {
				bubbles: true,
				cancelable: true,
				detail: {
					x: cameraEuler.x,
					y: cameraEuler.y,
					z: cameraEuler.z,
					d: cameraDistance
				}
			}));
		}
	},

	/**
	 * Enabled or disables user interaction
	 * @param {bool} interactionEnabled - The interaction state.
	 */
	setInteractionEnabled: function (interactionEnabled) {
		this._controls.enabled = interactionEnabled;
	},

	/**
	 * Loads all the resources of the model ( model, texture, filter colored-texture)
	 * Uses the ThreeJS loading manager, and initializes the ThreeJS entities when files are loaded
	 *
	 * @param {string} modelObjURL - URL of the .obj 3D model
	 * @param {string} textureURL - URL of the image of the main texture
	 * @param {string} textureColorURL - URL of the image of the colored-texture, used to hilite "areas" of the submodels (through shader)
	 */

	load3DModel: function (modelObjURL, textureURL, textureColorURL) {

		var t = this;

		t._isLoaded = false;

		var manager = new THREE.LoadingManager();
		manager.onProgress = function (item, loaded, total) {
			// console.log( "onProgress", item, loaded, total );
		};

		var vertShader = document.getElementById('phongVertexShader').textContent;
		var coloredFragShader = document.getElementById('phongFragmentShader').innerHTML;

		var textureLoader = new THREE.TextureLoader();

		// Texture du cerveau
		var brainTexture = textureLoader.load(this._brainFolder + textureURL, function (texture) {
		});

		// Texture pour le filtre par couleur
		var brainFilterColorTexture = textureLoader.load(this._brainFolder + textureColorURL, function (texture) {
		});

		var defaultColor = new THREE.Color("#000000");
		var defaultColorPercent = 0.0;
		var defaultOpacity = 1.0;
		var diffuseColor = 0xffffff;

		var defaultShaderUniforms = THREE.UniformsUtils.merge([
			THREE.ShaderLib.phong.uniforms,
			{ diffuse: { value: new THREE.Color(diffuseColor) } },
			{ specular: { value: new THREE.Color(0xFFFFFF) } },
			{ shininess: { value: 100.0} },
		]);

		defaultShaderUniforms.uBrainTexture = {type: "t", value: brainTexture};
		defaultShaderUniforms.uFilterColorTexture = {type: "t", value: brainFilterColorTexture};
		defaultShaderUniforms.uFilterColor = {type: "c", value: defaultColor};
		defaultShaderUniforms.uTintColor = {type: "c", value: defaultColor};
		defaultShaderUniforms.uTintColorPercent = {type: "f", value: defaultColorPercent};
		defaultShaderUniforms.uOpacity = {type: "f", value: defaultOpacity};

		var coloredShaderMaterial = new THREE.ShaderMaterial({
			uniforms: defaultShaderUniforms,
			vertexShader: vertShader,
			fragmentShader: coloredFragShader,
			lights: true
		});

		// model
		var onProgress = function (xhr) {
			if (xhr.lengthComputable) {
				var percent = Math.round(xhr.loaded / xhr.total * 100);
				if (percent === 100) {
					t._isLoaded = true;
					t.startRendering();
					t.domElement.dispatchEvent(new CustomEvent("brainLoadingComplete", {
						bubbles: true,
						cancelable: true
					}));
				} else {
					t._isLoaded = false;
					t.domElement.dispatchEvent(new CustomEvent("brainLoadingProgress", {
						bubbles: true,
						cancelable: true,
						detail: {
							percent: percent
						}
					}));
				}
			}
		};

		var onError = function (xhr) {
			console.log("Erreur import OBJ", xhr);
		};

		// CHARGEMENT du fichier .obj :
		var group = new THREE.Group();
		var loader = new THREE.OBJLoader(manager);
		var shaderClone, nodeState, nodeOpacity, nodeColor, nodeColorPercent, nodeRenderOrder;

		loader.load(this._brainFolder + modelObjURL, function (object) {

			// Normalisation des dimensions :
			// On veut placer le modèle dans une boîte entre -1 et 1

			// Dimensions du modèle 3D
			var boundingBox = new THREE.Box3().setFromObject(object);

			var maxDimensions = boundingBox.max;
			var minDimensions = boundingBox.min;
			var maxDimension = Math.max(maxDimensions.x - minDimensions.x,
				maxDimensions.y - minDimensions.y,
				maxDimensions.z - minDimensions.z);

			// Règle de trois pour avoir des dimensions entre -1 et +1 :
			var normScale = 2.0 / maxDimension;

			// Mise à l'échelle
			object.scale.set(normScale, normScale, normScale);

			object.traverse(function (node) {

				if (node instanceof THREE.Mesh) {

					// Par défaut :
					nodeOpacity = defaultOpacity;
					nodeColor = defaultColor;
					nodeColorPercent = defaultColorPercent;
					nodeRenderOrder = 0;

					nodeState = t.findNodeDefaultState(node.name);
					if (nodeState) {

						if (nodeState.color && nodeState.color.length === 7 && nodeState.color.indexOf("#") === 0) {
							nodeColor = new THREE.Color(nodeState.color);
							nodeColorPercent = 1.0;
						}

						if (nodeState.colorPercent) {
							nodeColorPercent = NS.Utilities.getFloatFromString(nodeState.colorPercent);
							nodeColorPercent = NS.Utilities.clamp(nodeColorPercent, 0, 1);
							console.log("JSON colorPercent", nodeColorPercent);
						}

						if (nodeState.opacity) {
							nodeOpacity = NS.Utilities.getFloatFromString(nodeState.opacity);
							nodeOpacity = NS.Utilities.clamp(nodeOpacity, 0, 1);
						}

						if (nodeState.renderOrder) {
							nodeRenderOrder = parseInt(nodeState.renderOrder);
						}
					}

					shaderClone = coloredShaderMaterial.clone();

					var shaderUniforms = THREE.UniformsUtils.merge([
						THREE.ShaderLib.phong.uniforms,
						{ diffuse: { value: new THREE.Color(diffuseColor) } },
						{ specular: { value: new THREE.Color(0xFFFFFF) } },
						{ shininess: { value: 100.0} },
					]);

					shaderUniforms.uBrainTexture = {type: "t", value: brainTexture};
					shaderUniforms.uFilterColorTexture = {type: "t", value: brainFilterColorTexture};
					shaderUniforms.uFilterColor = {type: "c", value: defaultColor};
					shaderUniforms.uTintColor = {type: "c", value: nodeColor};
					shaderUniforms.uTintColorPercent = {type: "f", value: nodeColorPercent};
					shaderUniforms.uOpacity = {type: "f", value: nodeOpacity};

					shaderClone.uniforms = shaderUniforms;

					node.material = shaderClone;
					node.material.transparent = nodeOpacity < 1;
					node.renderOrder = nodeRenderOrder;

					if (node.name !== "default") {
						// console.log("node", node.name, node);
					}

					// On stocke les références aux groupes
					t._sceneObjects[node.name] = node;

				}
			});

			group.add(object);

		}, onProgress, onError);

		this._scene.add(group);
	},

	findNodeDefaultState: function (nodeName) {
		var objectsState = this._objectsState;
		var i, objectState, n = objectsState.length;
		for (i = 0; i < n; i++) {
			objectState = objectsState[i];
			if (objectState.name === nodeName) {
				return objectState;
			}
		}
	},

	/**
	 * Returns true if the 3D files (obj, textures) are fully loaded, false otherwise
	 * Updated by the ThreeJS loading manager
	 *
	 * @return {bool} - The state of the view
	 */
	isLoaded: function () {
		return this._isLoaded;
	},



	/**
	 * Sets the opacity a subModel of the 3D Model
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 * - value : opacity value ( float from 0 to 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setOpacity: function (options) {

		var opacity = options.value;

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;

		// Modèle 3D de la partie du cerveau à modifier:
		var object3D = this.get3DObjectFromId(scenarioObjectId);
		if (object3D) {
			var materialShader = object3D.material;
			var materialShaderUniforms = materialShader.uniforms;
			materialShaderUniforms.uOpacity.value = opacity;

			materialShader.transparent = opacity < 1;
		}
	},


	/**
	 * Sets the color and eventually the opacity a subModel of the 3D Model
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 * - value : color value in hexadecimal format ( ex : #FFFFFF )
	 * - colorPercent : percentage of the color to mix with the color of the texture
	 * - opacity : opacity value ( float from 0 to 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setColor: function (options) {

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;

		var newColor = new THREE.Color(options.value);

		var colorPercent = options.colorPercent;
		colorPercent = NS.Utilities.getFloatFromString(colorPercent, 1.0);
		colorPercent = NS.Utilities.clamp(colorPercent, 0.0, 1.0);

		var opacity = options.opacity;
		opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
		opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

		// Modèle 3D de la partie du cerveau à modifier:
		var object3D = this.get3DObjectFromId(scenarioObjectId);
		if (object3D) {
			var materialShader = object3D.material;
			materialShader.transparent = opacity < 1;

			var materialShaderUniforms = materialShader.uniforms;
			materialShaderUniforms.uTintColor.value = newColor;
			materialShaderUniforms.uTintColorPercent.value = colorPercent;
			materialShaderUniforms.uOpacity.value = opacity;
		}
	},

	/**
	 * Reset color and the opacity of the submodel
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	restoreColor: function (options) {

		var scenarioObjectId = options.object;
		var object = this.get3DObjectFromId(scenarioObjectId);
		if (object) {
			// Paramètres de l'objet issus de brain.json
			var objectParameters = this.get3DObjectParametersFromId(scenarioObjectId);

			var color = objectParameters.color, defaultColor, colorPercent, opacity;
			if (color) {
				defaultColor = new THREE.Color(color);
				colorPercent = NS.Utilities.getFloatFromString(objectParameters.colorPercent, 0.0);
				colorPercent = NS.Utilities.clamp(colorPercent, 0.0, 1.0);
			} else {
				defaultColor = new THREE.Color("#FFFFFF");
				colorPercent = 0;
			}

			opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
			opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

			var object3D = this.get3DObjectFromId(scenarioObjectId);
			if (objectParameters && object3D) {
				var materialShader = object3D.material;
				var materialShaderUniforms = materialShader.uniforms;
				materialShaderUniforms.uTintColor.value = defaultColor;
				materialShaderUniforms.uTintColorPercent.value = colorPercent;
				materialShaderUniforms.uOpacity.value = opacity;

				materialShader.transparent = opacity < 1;
			}
		}
	},

	/**
	 * Reset color and the opacity of the all objects
	 * Called by the controller
	 *
	 * No options are defined at the moment
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	restoreAllColors: function ( options ) {
		var objectId;
		for (objectId in this._sceneIdToObject) {
			this.restoreColor({ object: objectId });
		}
	},

	/**
	 * Points the camera towards a subModel of the 3D Model
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 *
	 * The coordinates of the camera for this submodel are defined in the model description file :  brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	pointAt: function (options) {
		var scenarioObjectId = options.object;
		var object = this.get3DObjectFromId(scenarioObjectId);
		if (object) {
			// Paramètres de l'objet issus de brain.json
			var objectParameters = this.get3DObjectParametersFromId(scenarioObjectId);
			var object3D = this.get3DObjectFromId(scenarioObjectId);
			if (objectParameters && object3D) {
				this.startCameraRotation(
					objectParameters.x,
					objectParameters.y,
					objectParameters.z,
					objectParameters.d
				);
			}
		}
	},

	/**
	 * Points the camera towards a colored area of the 3D Model (cf hiliteArea)
	 * Called by the controller
	 *
	 * Options are :
	 * - area : ID of the area
	 *
	 * The coordinates of the camera for this area are defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	pointAtArea: function (options) {

		var areaId = options.area;
		var areaInfos = this.getAreaInfosFromId(areaId);
		if (areaInfos) {
			// Paramètres de l'objet issus de brain.json
			this.startCameraRotation(
				areaInfos.x,
				areaInfos.y,
				areaInfos.z,
				areaInfos.d
			);
		}
	},

	/**
	 * Hilites and Points the camera towards a colored area of the 3D Model (cf hiliteArea)
	 * Called by the controller
	 *
	 * Options are :
	 * - area : ID of the area
	 *
	 * The coordinates of the camera for this area are defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	hiliteAndPointAtArea: function (options) {
		this.hiliteArea(options);
		this.pointAtArea(options);
	},

	/**
	 * Hilites a submodel by :
	 * - setting the opacity a subModel to 1
	 * - and setting the opacity of others to 0.5, adding transparency to those
	 *
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	hilite: function (options) {

		var scenarioObjectId = options.object;

		var objectId, opacity;
		for (objectId in this._sceneIdToObject) {
			opacity = objectId === scenarioObjectId ? 1 : 0.5;

			// Modèle 3D de la partie du cerveau à modifier:
			var object3D = this.get3DObjectFromId(objectId);
			if (object3D) {
				var materialShader = object3D.material;
				var materialShaderUniforms = materialShader.uniforms;
				materialShaderUniforms.uOpacity.value = opacity;

				materialShader.transparent = opacity < 1;
			}
		}
	},

	/**
	 * Hilites and Points the camera towards a part of the 3D Model (cf hilite)
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 *
	 * The coordinates of the camera for this area are defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	hiliteAndPointAt: function (options) {
		this.hilite(options);
		this.pointAt(options);
	},

	/**
	 * Actives the shader color filter to hilite an area of 3D submodels
	 * Called by the controller
	 *
	 * Options are :
	 * - area : ID of the area
	 *
	 * The color applied to the area is defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	hiliteArea: function (options) {

		var areaId = options.area;

		var infos = this.getAreaInfosFromId(areaId);
		if (infos && infos.filterColor) {
			var areaColor = new THREE.Color(infos.filterColor);

			var objectId;
			for (objectId in this._sceneIdToObject) {
				var object3D = this.get3DObjectFromId(objectId);
				if (object3D) {
					var materialShader = object3D.material;
					var materialShaderUniforms = materialShader.uniforms;
					materialShaderUniforms.uFilterColor.value = areaColor;
				}
			}
		}
	},

	/**
	 * Disables the shader color filter used to hilite an area of 3D submodels
	 *
	 * Called by the controller
	 * No options are defined at the moment
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */

	restoreAllAreas: function ( options ) {

		var blackColor = new THREE.Color('#000000');

		var objectId;
		for (objectId in this._sceneIdToObject) {
			var object3D = this.get3DObjectFromId(objectId);
			if (object3D) {
				var materialShader = object3D.material;
				var materialShaderUniforms = materialShader.uniforms;
				materialShaderUniforms.uFilterColor.value = blackColor;
			}
		}
	},

	/**
	 * Reset the 3D-model view : combines restoreAllAreas and restoreAllColors
	 *
	 * Called by the controller
	 * No options are defined at the moment
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */

	restoreBrainView: function ( options ) {
		this.restoreAllAreas(options);
		this.restoreAllColors(options);
	},

	/**
	 * Moves the camera
	 * Called by the controller
	 *
	 * Options are :
	 * - x : Euler angle along X axis, in degrees
	 * - y : Euler angle along Y axis, in degrees
	 * - z : Euler angle along Z axis, in degrees
	 * - d: distance to the center of the brain 3D model (normalized in a -1/+1 box)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setCamera: function (options) {
		this.startCameraRotation (options.x, options.y, options.z, options.d);
	},

	/**
	 * Reset the initial value of the quaternion associated with the camera
	 * Defines the starting position/rotation of the the next transition
	 */
	resetCameraRotation: function () {
	    this.cameraQ = this._camera.quaternion;
    },

	/**
	 * Starts a transition to move the camera
	 *
	 * @param [number] angleDegX - Euler angle along X axis, in degrees
	 * @param [number] angleDegY - Euler angle along Y axis, in degrees
	 * @param [number] angleDegZ - Euler angle along Z axis, in degrees
	 * @param [number] distance - distance to the center of the brain 3D model
	 *
	 */
	startCameraRotation: function (angleDegX, angleDegY, angleDegZ, distance) {

		var degToRad = Math.PI / 180.0;

		var angleX = angleDegX * degToRad; // Angle de rotation sur l'axe des X (radians)
		var angleY = angleDegY * degToRad; // Angle de rotation sur l'axe des Y (radians)
		var angleZ = angleDegZ * degToRad; // Angle de rotation sur l'axe des Z (radians)

		// Position initiale : distance et quaternion de la rotation
		this.iniR = this._camera.position.length();
		this.iniQ = new THREE.Quaternion().copy(this.cameraQ);

		// Position finale : distance et quaternion de la rotation
		this.endR = distance;
		this.endQ = new THREE.Quaternion().setFromEuler(new THREE.Euler(angleX, angleY, angleZ, 'XYZ'));

		// Quaternion où on enregistre l'interpolation
		this.curQ = new THREE.Quaternion();

		// Position de la caméra
		this.vec3 = new THREE.Vector3();

		var t = this;
        var animationParams = { percent: 0 };

        var tween = new TWEEN.Tween( animationParams )
            .to({ percent: 1 }, 1000)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate(function() {
                t.slerpCameraRotation( animationParams.percent );
            })
            .onComplete(function() {
                // On mémorise le quaternion définissant la rotation courante de la caméra
                t.resetCameraRotation();
            })
            .start();
	},

	/**
	 * Updates the transition moving the camera
	 *
	 * Computes a intermediary quaternion between the initial and the final ones
	 * Applies the quartenion-based coordinates to the camera, and points the camera to the center of the model
	 *
	 * @param [number] percent - percentage of the transition (from 0 to 1)
	 *
	 */
	slerpCameraRotation: function(percent){

		// Animation de la rotation
		THREE.Quaternion.slerp(this.iniQ, this.endQ, this.curQ, percent);

		// On applique le quaternion à la position initiale de la caméra
		this.vec3.x = this.cameraPos0.x;
		this.vec3.y = this.cameraPos0.y;
		this.vec3.z = this.cameraPos0.z;
		this.vec3.applyQuaternion(this.curQ);

		// Animation de la distance
		var r = this.iniR + percent * ( this.endR - this.iniR );
		this.vec3.multiplyScalar( r / this.vec3.length() );

		this._camera.position.copy(this.vec3);
		this._camera.lookAt(this.origin);

		// On applique le quaternion à l'orientation de la caméra
		this.vec3 = this.cameraUp0.clone();
		this.vec3.applyQuaternion(this.curQ);
		this._camera.up.copy(this.vec3);
	}
};

